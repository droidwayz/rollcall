import { open } from 'sqlite';
import sqlite3 from 'sqlite3';

export default class Database {
    async open(filename) {
        const db = await open({ filename, driver: sqlite3.Database });
        await db.migrate();

        // MEMBER QUERIES
        this.insertMember = await db.prepare('INSERT INTO members (id) VALUES (?);');
        this.getMember = await db.prepare('SELECT * FROM members WHERE id = ?;');
        this.updateTimezone = await db.prepare('UPDATE members SET timezone = ? WHERE id = ?;');

        // EVENT QUERIES
        this.createEvent = await db.prepare('INSERT INTO events (owner, start, title, created_at) VALUES (?, ?, ?, 0);');
        this.updateEventTime = await db.prepare('UPDATE events SET start = ? WHERE id = ?;');
        this.dropEvent = await db.prepare('UPDATE events SET deleted_at = ? WHERE id = ?;');
        this.getEvent = await db.prepare('SELECT * FROM events WHERE id = ?;');
        this.getUpcomingEvents = await db.prepare('SELECT * FROM events WHERE start > strftime("%s", "now") AND deleted_at IS NULL;');
        this.getUpcomingMemberEvents = await db.prepare('SELECT * FROM events INNER JOIN event_attendees ON events.id = event_attendees.event_id WHERE start > strftime("%s", "now") AND event_attendees.member = ? AND deleted_at IS NULL;');

        // ATTENDEE QUERIES
        this.addEventAttendee = await db.prepare('INSERT INTO event_attendees (event_id, member) VALUES (?, ?);');
        this.removeEventAttendee = await db.prepare('DELETE FROM event_attendees WHERE event_id = ? AND member = ?;');
        this.getEventAttendee = await db.prepare('SELECT * FROM event_attendees WHERE event_id = ? AND member = ?;');
        this.getEventAttendees = await db.prepare('SELECT * FROM event_attendees WHERE event_id = ?');
    }
}
