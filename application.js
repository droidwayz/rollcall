import moment from 'moment-timezone';
import { MessageEmbed, DMChannel } from 'discord.js';

export default class Application {
    constructor(client, db) {
        this.client = client;
        this.db = db;

        this.commands = {
            'help': {
                command: 'help',
                help: 'This message',
                callback: this.help
            },
            'ping': {
                command: 'ping',
                help: 'Pong!',
                callback: this.ping
            },
            'update timezone': {
                command: 'update timezone',
                help: 'Update your timezone (format: "update timezone <timezone name>")',
                input: '(?<timezone>[\\w]+/[\\w]+)',
                callback: this.updateTimezone
            },
            'my timezone': {
                command: 'my timezone',
                help: 'Show your current timezone',
                callback: this.showTimezone
            },
            'create event': {
                command: 'create event',
                help: 'Create an event (format: "create event on <date> named <title>"), see https://momentjs.com/guides/#/parsing/ for how to format the date',
                input: 'on (?<date>.+) named (?<title>.+)',
                callback: this.createEvent
            },
            'drop event': {
                command: 'drop event',
                help: 'Drop an event (format: "drop event <id>")',
                input: '(?<dropped_id>[\\d]+)',
                callback: this.dropEvent
            },
            'update event time': {
                command: 'update event time',
                help: 'Update an event\'s time to a new one (format: "update event time for <id> to <date>"), see https://momentjs.com/guides/#/parsing/ for how to format the date',
                input: 'for (?<updated_id>[\\d]+) to (?<updated_date>.+)',
                callback: this.updateEventTime
            },
            'show event': {
                command: 'show event',
                help: 'Show an event by its id (format: "show event <id>")',
                input: '(?<event_id>[\\d]+)',
                callback: this.showEvent
            },
            'upcoming events': {
                command: 'upcoming events',
                help: 'Show all upcoming events',
                callback: this.upcomingEvents
            },
            'sign up for': {
                command: 'sign up for',
                help: 'Sign up for an event (format: "sign up for <id>")',
                input: '(?<signup_id>[\\d]+)',
                callback: this.signup
            },
            'drop out of': {
                command: 'drop out of',
                help: 'Drop out of an event (format: "drop out of <id>")',
                input: '(?<dropout_id>[\\d]+)',
                callback: this.dropout
            },
            'my events': {
                command: 'my events',
                help: 'Show your upcoming events',
                callback: this.showEvents
            }
        };

        const regex = Object.values(this.commands).map(c => {
            return '(' + c.command + ('input' in c ? ') ' + c.input : ')');
        }).join('|');
        this.commandRegex = new RegExp(`(${regex})`);
    }

    formatTimestamp(ts, tz) {
        return moment.unix(ts).tz(tz).format('dddd, MMMM Do YYYY, h:mma');
    }

    async buildEventEmbed(client, timezone, evt, attendees) {
        const owner = await client.users.fetch(evt.owner);

        const reply = new MessageEmbed()
            .setAuthor(owner.tag)
            .setTitle(`${evt.title} (#${evt.id})`)
            .setDescription(`Starting at ${this.formatTimestamp(evt.start, timezone)}`);

        if (attendees.length > 0) {
            const all = await Promise.all(attendees.map(a => client.users.fetch(a.member)));
            reply.addField('Attendees', all.join("\n"));
        }

        return reply;
    }

    async init(token) {
        this.client.on('ready', () => {
            console.log(`Connected as ${this.client.user.tag} (${this.client.user.id})`);
        });
        this.client.on('message', msg => this.onMessage(msg));

        this.client.login(token);
    }

    async help(msg) {
        msg.author.send(`Help:\n${Object.values(this.commands).map(c => `${c.command}: ${c.help}`).join("\n")}`);
    }

    async ping(msg) {
        msg.reply('pong!');
    }

    async updateTimezone(msg, args) {
        await this.db.insertMember.run(msg.author.id);
        await this.db.updateTimezone.run(args.timezone, msg.author.id);

        msg.reply(`OK, I set your timezone to ${args.timezone}`);
    }

    async showTimezone(msg) {
        await this.db.insertMember.run(msg.author.id);
        const member = await this.db.getMember.get(msg.author.id);
        console.log(member);

        msg.reply(`Your timezone is set to ${member.timezone}`);
    }

    async createEvent(msg, args) {
        const date = args.date;
        const title = args.title;

        await this.db.insertMember.run(msg.author.id);
        const member = await this.db.getMember.get(msg.author.id);

        const time = moment.tz(date, member.timezone);

        if (!time.isValid()) {
            msg.reply(`Sorry, your entered time of ${date} is invalid. I haven't created anything.`);
            return;
        }

        const inserted = await this.db.createEvent.run(msg.author.id, time.unix(), title);

        const evt = await this.db.getEvent.get(inserted.lastID);
        await this.db.addEventAttendee.run(inserted.lastID, msg.author.id);

        msg.reply(`OK! I created your event, ${evt.title} (#${inserted.lastID})`);
    }

    async showEvent(msg, args) {
        const id = args.event_id;
        const member = await this.db.getMember.get(msg.author.id);
        const evt = await this.db.getEvent.get(id);
        const attendees = await this.db.getEventAttendees.all(id);

        const reply = await this.buildEventEmbed(msg.client, member.timezone, evt, attendees);
        msg.channel.send(reply);
    }

    async signup(msg, args) {
        const id = args.signup_id;
        const user = msg.author.id;

        await this.db.insertMember.run(user);

        const evt = await this.db.getEvent.get(id);
        const alreadySignedUp = await this.db.getEventAttendee.get(user);

        if (evt.owner !== user && !alreadySignedUp) {
            await this.db.addEventAttendee.run(id, user);

            msg.reply(`OK! You're now signed up for ${evt.title}.`);
        } else {
            msg.reply(`Sorry, you're either the event owner or you're not signed up for ${evt.title}`)
        }
    }

    async dropout(msg, args) {
        const id = args.dropout_id;
        const user = msg.author.id;

        await this.db.insertMember.run(user);
        const evt = await this.db.getEvent.get(id);
        const isSignedUp = await this.db.getEventAttendee.get(user);

        if (evt.owner !== user && isSignedUp) {
            await this.db.removeEventAttendee.run(id, user);

            msg.reply(`OK, you're no longer signed up for ${evt.title}. Sorry to see you go!`);
        } else {
            msg.reply(`Sorry, you're either the event owner or you're not signed up for ${evt.title}`);
        }
    }

    async showEvents(msg, args) {
        const user = msg.author.id;
        await this.db.insertMember.run(user);
        const member = await this.db.getMember.get(msg.author.id);

        let reply = `Your upcoming events:\n`;
        await this.db.getUpcomingMemberEvents.each(user, (err, evt) => {
            reply += `* ${evt.title} (#${evt.id}) on ${this.formatTimestamp(evt.start, member.timezone)}\n`;
        });
        msg.reply(reply);
    }

    async upcomingEvents(msg, args) {
        const member = await this.db.getMember.get(msg.author.id);

        let reply = `All upcoming events:\n`;
        await this.db.getUpcomingEvents.each((err, evt) => {
            reply += `* ${evt.title} (#${evt.id}) on ${this.formatTimestamp(evt.start, member.timezone)}\n`;
        });
        msg.channel.send(reply);
    }

    async dropEvent(msg, args) {
        const id = args.dropped_id;
        await this.db.insertMember.run(msg.author.id);
        const evt = await this.db.getEvent.get(id);
        if (evt) {
            const dropped = await this.db.dropEvent.run(Date.now()/1000, id);
            if (dropped.changes >= 1) {
                msg.reply(`OK, I've marked ${evt.title} as dropped.`);
            } else {
                msg.reply(`I failed to find an event with id ${id}, are you sure you have the right event?`);
            }
        } else {
            msg.reply(`Sorry, I couldn't find ${id}, are you sure you have the right event?`);
        }
    }

    async updateEventTime(msg, args) {
        const id = args.updated_id;
        const date = args.updated_date;

        await this.db.insertMember.run(msg.author.id);

        const evt = await this.db.getEvent.get(id);
        if (evt) {
            const member = await this.db.getMember.get(msg.author.id);
            const time = moment.tz(date, member.timezone);
            if (!time.isValid()) {
                msg.reply(`Sorry, your entered time of ${date} is invalid. I haven't updated anything.`);
            } else {
                const updated = await this.db.updateEventTime.run(time.unix(), id);
                if (updated.changes >= 1) {
                    msg.reply(`OK, I've updated the event time to ${this.formatTimestamp(time, member.timezone)}`);
                } else {
                    msg.reply(`Sorry, I couldn't find ${id}, are you sure you have the right event?`);
                }
            }
        } else {
            msg.reply(`Sorry, I couldn't find ${id}, are you sure you have the right event?`);
        }
    }

    onMessage(msg) {
        if (
            (
                msg.content.startsWith(`<@!${this.client.user.id}>`) ||
                msg.content.startsWith(`<@${this.client.user.id}>`) ||
                msg.channel instanceof DMChannel
            ) &&
            !msg.author.bot
        ) {
            console.log('Heard message', msg.content);
            const input = msg.content;
            const parsed = input.match(this.commandRegex);
            if (parsed !== null) {
                const command = parsed.filter(x => !!x)[2];
                const args = parsed.groups;
                const exec = this.commands[command];
                console.log('Tried to find command', command, '(', this.commands[command], ')');
                exec.callback.call(this, msg, args)
                    .catch(e => {
                        console.log(e);
                        msg.reply(`Sorry, I failed to execute "${command}"!`);
                    });
            }
        }
    }
};
