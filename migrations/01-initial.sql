--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS members (
    id TEXT NOT NULL PRIMARY KEY ASC ON CONFLICT IGNORE,
    timezone TEXT NOT NULL DEFAULT 'UTC'
);

CREATE TABLE IF NOT EXISTS events (
    id INTEGER NOT NULL PRIMARY KEY ASC ON CONFLICT ROLLBACK AUTOINCREMENT,
    owner TEXT NOT NULL REFERENCES members (id),
    title TEXT NOT NULL,
    start INTEGER NOT NULL,
    created_at INTEGER NOT NULL,
    updated_at INTEGER,
    deleted_at INTEGER
);

CREATE TABLE IF NOT EXISTS event_attendees (
    id INTEGER NOT NULL PRIMARY KEY ASC AUTOINCREMENT,
    event_id INTEGER NOT NULL REFERENCES events (id),
    member TEXT NOT NULL REFERENCES members (id),
    CONSTRAINT one_member_per_event UNIQUE (event_id, member)
);

CREATE TRIGGER IF NOT EXISTS created_at AFTER INSERT ON events
BEGIN
    UPDATE events SET created_at = strftime('%s', 'now') WHERE id = new.id;
END;

CREATE TRIGGER IF NOT EXISTS updated_at AFTER UPDATE OF owner, start, deleted_at ON events
BEGIN
    UPDATE events SET updated_at = strftime('%s', 'now') WHERE id = old.id;
END;

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS members;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS event_attendees;

DROP TRIGGER IF EXISTS created_at;
DROP TRIGGER IF EXISTS updated_at;
